package com.immobilienscout24.technical.challenge.dto;

public class HeaderCounterResponse {
    private String name;
    private Integer amount;

    public HeaderCounterResponse() {
    }

    public HeaderCounterResponse(String name, Integer amount) {
        this.name = name;
        this.amount = amount;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }
}

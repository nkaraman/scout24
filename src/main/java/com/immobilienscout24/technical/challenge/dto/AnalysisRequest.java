package com.immobilienscout24.technical.challenge.dto;

/**
 *
 */
public class AnalysisRequest {
    private String url;
    private boolean processJavaScript;
    private int maxVisitPages = 10;
    private boolean checkAvailability;
    private Integer politenessDelayMilliseconds;
    private Integer timeoutMilliseconds;
    private Integer globalTimeoutMilliseconds;

    public String getUrl() {
        return url;
    }

    public boolean isProcessJavaScript() {
        return processJavaScript;
    }

    public int getMaxVisitPages() {
        return maxVisitPages;
    }

    public boolean isCheckAvailability() {
        return checkAvailability;
    }

    public Integer getPolitenessDelayMilliseconds() {
        return politenessDelayMilliseconds;
    }

    public Integer getTimeoutMilliseconds() {
        return timeoutMilliseconds;
    }

    public Integer getGlobalTimeoutMilliseconds() {
        return globalTimeoutMilliseconds;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public void setProcessJavaScript(boolean processJavaScript) {
        this.processJavaScript = processJavaScript;
    }

    public void setMaxVisitPages(int maxVisitPages) {
        this.maxVisitPages = maxVisitPages;
    }

    public void setCheckAvailability(boolean checkAvailability) {
        this.checkAvailability = checkAvailability;
    }

    public void setPolitenessDelayMilliseconds(Integer politenessDelayMilliseconds) {
        this.politenessDelayMilliseconds = politenessDelayMilliseconds;
    }

    public void setTimeoutMilliseconds(Integer timeoutMilliseconds) {
        this.timeoutMilliseconds = timeoutMilliseconds;
    }

    public void setGlobalTimeoutMilliseconds(Integer globalTimeoutMilliseconds) {
        this.globalTimeoutMilliseconds = globalTimeoutMilliseconds;
    }
}

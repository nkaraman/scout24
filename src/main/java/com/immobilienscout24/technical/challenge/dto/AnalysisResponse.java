package com.immobilienscout24.technical.challenge.dto;

import java.util.*;

/**
 * Represents output information for an analysis request
 */
public class AnalysisResponse {
    private PageResponse page;
    private List<HeaderCounterResponse> headingCounter = Collections.emptyList();
    private List<LinkedResourceResponse> internalLinks = Collections.emptyList();
    private List<LinkedResourceResponse> externalLinks = Collections.emptyList();
    private boolean loginForm;

    public AnalysisResponse() {
    }

    public AnalysisResponse(String url, String errorMessage) {
        page = new PageResponse();
        page.setUrl(url);
        page.setErrorMessage(errorMessage);
    }

    public PageResponse getPage() {
        return page;
    }

    public void setPage(PageResponse page) {
        this.page = page;
    }

    public List<HeaderCounterResponse> getHeadingCounter() {
        return headingCounter;
    }

    public void setHeadingCounter(List<HeaderCounterResponse> headingCounter) {
        this.headingCounter = headingCounter;
    }

    public List<LinkedResourceResponse> getInternalLinks() {
        return internalLinks;
    }

    public void setInternalLinks(List<LinkedResourceResponse> internalLinks) {
        this.internalLinks = internalLinks;
    }

    public List<LinkedResourceResponse> getExternalLinks() {
        return externalLinks;
    }

    public void setExternalLinks(List<LinkedResourceResponse> externalLinks) {
        this.externalLinks = externalLinks;
    }

    public boolean isLoginForm() {
        return loginForm;
    }

    public void setLoginForm(boolean loginForm) {
        this.loginForm = loginForm;
    }
}

package com.immobilienscout24.technical.challenge.controller;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;

@Controller
@ControllerAdvice
public class ErrorController implements org.springframework.boot.autoconfigure.web.ErrorController{
    private static final Logger LOGGER = LoggerFactory.getLogger(ErrorController.class);

    @Override
    public String getErrorPath() {
        return "error";
    }

    @ExceptionHandler(value = Exception.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public ModelAndView defaultErrorHandler(HttpServletRequest request, Exception e)
    {
        LOGGER.error("Request " + request.getRequestURI() + " failed", e);

        return new ModelAndView("error");
    }
}

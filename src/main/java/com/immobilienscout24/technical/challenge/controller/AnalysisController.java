package com.immobilienscout24.technical.challenge.controller;

import com.immobilienscout24.technical.challenge.dto.AnalysisRequest;
import com.immobilienscout24.technical.challenge.dto.AnalysisResponse;
import com.immobilienscout24.technical.challenge.service.AnalysisService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.util.HashMap;
import java.util.Map;

/**
 *
 */
@Controller
public class AnalysisController {
    @Autowired
    private AnalysisService analysisService;

    @PostMapping(value = "api/analyse")
    @ResponseBody
    public AnalysisResponse analyse(@RequestBody AnalysisRequest analysis) {
        return analysisService.analyse(analysis);
    }


    @RequestMapping(value = "/")
    public ModelAndView index(@RequestParam(required = false, value = "url")
                                      String url,
                              @RequestParam(required = false, value = "processJavaScript", defaultValue = "false")
                                      Boolean processJavaScript,
                              @RequestParam(required = false, value = "maxVisitPages", defaultValue = "10")
                                      Integer maxVisitPages,
                              @RequestParam(required = false, value = "checkAvailability", defaultValue = "false")
                                      Boolean checkAvailability,
                              @RequestParam(required = false, value = "politenessDelayMilliseconds", defaultValue = "1000")
                                      Integer politenessDelayMilliseconds,
                              @RequestParam(required = false, value = "timeoutMilliseconds", defaultValue = "1000")
                                      Integer timeoutMilliseconds,
                              @RequestParam(required = false, value = "globalTimeoutMilliseconds", defaultValue = "20000")
                                      Integer globalTimeoutMilliseconds) {
        Map<String, Object> model = new HashMap<>();
        if (url != null) {
            AnalysisRequest analysis = new AnalysisRequest();
            analysis.setUrl(url);
            analysis.setProcessJavaScript(processJavaScript);
            analysis.setCheckAvailability(checkAvailability);
            analysis.setMaxVisitPages(maxVisitPages);
            analysis.setPolitenessDelayMilliseconds(politenessDelayMilliseconds);
            analysis.setTimeoutMilliseconds(timeoutMilliseconds);
            analysis.setGlobalTimeoutMilliseconds(globalTimeoutMilliseconds);

            fulfillModelAndView(model, analysis);
        }
        return new ModelAndView("index", model);
    }

    private void fulfillModelAndView(Map<String, Object> model, AnalysisRequest analysis) {
        AnalysisResponse response = analyse(analysis);

        model.put("url", response.getPage().getUrl());
        model.put("htmlVersion", response.getPage().getHtmlVersion());
        model.put("title", response.getPage().getTitle());
        model.put("isLoginForm", response.isLoginForm());
        model.put("errorMessage", response.getPage().getErrorMessage());

        model.put("headingCounter", response.getHeadingCounter());
        model.put("internalLinks", response.getInternalLinks());
        model.put("internalLinksNumber", response.getInternalLinks().size());
        model.put("externalLinks", response.getExternalLinks());
        model.put("externalLinksNumber", response.getExternalLinks().size());
    }

}

package com.immobilienscout24.technical.challenge.service;

import com.gargoylesoftware.htmlunit.html.HtmlAnchor;
import com.gargoylesoftware.htmlunit.html.HtmlElement;
import com.gargoylesoftware.htmlunit.html.HtmlPage;
import com.immobilienscout24.technical.challenge.dto.*;
import edu.uci.ics.crawler4j.url.URLCanonicalizer;

import org.apache.commons.lang3.tuple.Pair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StopWatch;
import org.w3c.dom.DocumentType;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.*;
import java.util.stream.Collectors;

/**
 *
 */
@Service
public class AnalysisService {
    private static final String HTTP = "http";
    private static final Logger LOGGER = LoggerFactory.getLogger(AnalysisService.class);

    private final UrlFetcher urlFetcher;

    @Autowired
    public AnalysisService(UrlFetcher urlFetcher) {
        this.urlFetcher = urlFetcher;
    }

    public AnalysisResponse analyse(AnalysisRequest analysisRequest) {
        StopWatch sw = new StopWatch("Analyse for url " + analysisRequest.getUrl() + " js is " + analysisRequest.isProcessJavaScript() );
        Set<String> urls;
        sw.start("Validation&URI");
        Pair<URI, AnalysisResponse> uriPair = validateAndBuildUri(analysisRequest.getUrl());
        if (uriPair.getLeft() == null) {
            return uriPair.getRight();
        }
        sw.stop();
        sw.start("Fetch&Process HTML");
        AnalysisResponse analyseResponse = new AnalysisResponse();
        try {
            HtmlPage page = urlFetcher.fetchAndProcessHtml(analysisRequest.getUrl(), analysisRequest.getTimeoutMilliseconds(), analysisRequest.isProcessJavaScript());
            if (page == null) {
                return new AnalysisResponse(analysisRequest.getUrl(),
                        "It is not an HTML page");
            }
            analyseResponse.setPage(generatePageResponse(page));
            analyseResponse.setHeadingCounter(countHeaders(page));
            analyseResponse.setLoginForm(isItLoginForm(page));

            urls = page.getAnchors().stream().map(HtmlAnchor::getHrefAttribute).map(url -> URLCanonicalizer.getCanonicalURL(url, page.getUrl().toString()))
                    .filter(Objects::nonNull).collect(Collectors.toSet());
        } catch (IOException e) {
            return new AnalysisResponse(analysisRequest.getUrl(),
                    "Page failed to load with exception: " + e.getMessage());
        }
        sw.stop();
        sw.start("Process linked resources");

        Pair<List<LinkedResourceResponse>, List<LinkedResourceResponse>> linkedResources = checkLinks(analysisRequest, urls, uriPair.getLeft().getHost());
        analyseResponse.setInternalLinks(linkedResources.getLeft());
        analyseResponse.setExternalLinks(linkedResources.getRight());
        sw.stop();

        LOGGER.info(sw.prettyPrint());
        return analyseResponse;
    }

    private PageResponse generatePageResponse(HtmlPage page) {
        PageResponse pageResponse = new PageResponse();
        pageResponse.setUrl(page.getUrl().toString());

        DocumentType doctype = page.getDoctype();
        if (doctype != null) {
            String htmlVersion = doctype.getName();
            if(doctype.getPublicId() != null) {
                htmlVersion += " " + doctype.getPublicId();
            }
            pageResponse.setHtmlVersion(htmlVersion);
        }

        pageResponse.setTitle(page.getTitleText());
        return pageResponse;
    }

    Pair<List<LinkedResourceResponse>, List<LinkedResourceResponse>> checkLinks(AnalysisRequest analysisRequest,
                                                                                        Set<String> urls, String domain) {
        List<LinkedResourceResponse> internals = new ArrayList<>();
        List<LinkedResourceResponse> externals = new ArrayList<>();
        StopWatch sw = new StopWatch("checkLinks");
        sw.start("URI building");
        Pair<List<URI>, List<LinkedResourceResponse>> urisPair = buildHttpsUris(urls);
        internals.addAll(urisPair.getRight());
        sw.stop();
        sw.start("urlFetcher.fetchResources");
        Map<URI, Optional<String>> urlToErrorMessageMap = urlFetcher.fetchResources(urisPair.getLeft(), analysisRequest);
        for (URI uri : urlToErrorMessageMap.keySet()) {
            Optional<String> errorMessage = urlToErrorMessageMap.get(uri);

            LinkedResourceResponse resource = new LinkedResourceResponse();
            resource.setUrl(uri.toString());
            resource.setErrorMessage(errorMessage.orElse(null));
            resource.setReached(!errorMessage.isPresent());

            String host = uri.getHost();
            if (host.contains(domain)) {
                internals.add(resource);
            } else {
                externals.add(resource);
            }
        }
        sw.stop();
        LOGGER.info(sw.prettyPrint());
        return Pair.of(internals, externals);
    }

    /**
     * @param urls
     * @return left -> URIs that converted and right -> LinkedResources for urls with errors
     */
    Pair<List<URI>, List<LinkedResourceResponse>> buildHttpsUris(Set<String> urls) {
        List<URI> uriToCheckAvailability = new ArrayList<>();
        List<LinkedResourceResponse> resourcesWithErrors = new ArrayList<>();
        for (String url : urls) {
            try {
                URI uri = new URI(url);
                if (uri.getScheme().startsWith(HTTP)) {
                    uriToCheckAvailability.add(new URI(url));
                } else {
                    throw new UnsupportedOperationException("Protocol " + uri.getScheme() + " is not supported");
                }
            } catch (URISyntaxException | UnsupportedOperationException e) {
                LinkedResourceResponse resource = new LinkedResourceResponse();
                resource.setUrl(url);
                resource.setReached(false);
                resource.setErrorMessage(e.getMessage());
                resourcesWithErrors.add(resource);
            }
        }

        return Pair.of(uriToCheckAvailability, resourcesWithErrors);
    }

    Pair<URI, AnalysisResponse> validateAndBuildUri(String url) {

        try {
            URI uri = new URI(url);
            if (uri.getScheme() != null && uri.getScheme().startsWith(HTTP)) {
                return Pair.of(uri, null);
            } else {
                return Pair.of(null, new AnalysisResponse(url,
                        "Protocol " + uri.getScheme() + " is not supported"));
            }
        } catch (URISyntaxException e) {
            return Pair.of(null, new AnalysisResponse(url,
                    "Url is not valid. Please check url standard http://www.ietf.org/rfc/rfc2396.txt"));
        }
    }

    private List<HeaderCounterResponse> countHeaders(HtmlPage page) {
        List<HeaderCounterResponse> results = new ArrayList<>();
        for (int i = 1; i <= 6; i++) {
            results.add(new HeaderCounterResponse("h" + i, page.getByXPath("//h" + i).size()));
        }
        return results;
    }

    /**
     *
     * Would work correctly for pages if:
     * - login form is not always visible (for some websites it does not require log in, but the form is presented on all pages)
     * - login form is not consist of two steps (would not work with Web Skype)
     *
     * @param page to verify
     * @return true if it is a login page
     */
    private boolean isItLoginForm(HtmlPage page) {
        List<HtmlElement> elements = (List<HtmlElement>) page.getByXPath("//input[@type='password']");
        return !elements.isEmpty();
    }
}

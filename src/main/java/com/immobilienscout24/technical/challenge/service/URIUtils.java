package com.immobilienscout24.technical.challenge.service;

import org.springframework.stereotype.Service;

import java.io.IOException;
import java.net.URI;
import java.net.URLConnection;

/**
 * This class needed for testing purposes.
 * URL, URI is final and not possible to use with Mockito.
 * I could use PowerMock. But this solution is simpler.
 *
 */
@Service
public class URIUtils {
    URLConnection openConnection(URI uri) throws IOException {
        return uri.toURL().openConnection();
    }
}

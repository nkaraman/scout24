package com.immobilienscout24.technical.challenge.service;

import com.gargoylesoftware.htmlunit.Page;
import com.gargoylesoftware.htmlunit.WebClient;
import com.gargoylesoftware.htmlunit.html.HtmlPage;
import com.gargoylesoftware.htmlunit.javascript.background.JavaScriptJobManager;

import com.immobilienscout24.technical.challenge.dto.AnalysisRequest;
import org.apache.commons.lang3.ObjectUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StopWatch;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URI;
import java.net.URLConnection;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

/**
 * I took into account that it is suggested to use JSoup.
 * However, JSoup does not work with dynamic HTML that is generated through JS (this is wide spread).
 * <p>
 * <p>
 * HtmlUnit on the other hand does support JS, because it is headless browser.
 * In the same way it supports XPath (and I would not need to add html-parser).
 * <p>
 * Created by astral on 21.07.17.
 */
@Service
public class UrlFetcher {
    private final static Logger LOGGER = LoggerFactory.getLogger(UrlFetcher.class);
    private static final int DEFAULT_TIMEOUT_MILLISECONDS = 500;
    private static final int DEFAULT_POLITENESS_DELAY_MILLISECONDS = 500;
    private static final int DEFAULT_GLOBAL_TIMEOUT_MILLISECONDS = 20_000;
    private static final String HTTP = "http";

    private final URIUtils uriUtils;

    @Autowired
    public UrlFetcher(URIUtils uriUtils) {
        this.uriUtils = uriUtils;
    }

    public HtmlPage fetchAndProcessHtml(String url, Integer timeoutMilliseconds, boolean processJS) throws IOException {
        if (!url.startsWith(HTTP)) {
            url = HTTP + url;
        }
        StopWatch sw = new StopWatch("Fetch&Process page " + url + " js is " + processJS);
        sw.start("WebDriver init");
        WebClient webClient = init(timeoutMilliseconds, processJS);
        sw.stop();
        sw.start("Getting html page with dependencies");
        HtmlPage htmlPage = fetchHtmlPage(url, timeoutMilliseconds, webClient, processJS);
        sw.stop();
        LOGGER.info(sw.prettyPrint());
        webClient.close();
        return htmlPage;
    }

    /**
     * Performance improvement possible:
     * - should limit amount of threads per request;
     * - raise the ulimit on a server to avoid "Too many open files"
     * - queue for incoming calls if we have too many
     * - workers
     *
     * @param uris expected to have distinct urls
     * @return url -> errorMessage
     */
    public Map<URI, Optional<String>> fetchResources(List<URI> uris, AnalysisRequest analyse) {
        final Map<URI, Optional<String>> results = new ConcurrentHashMap<>();
        //for a test project we could not limit ourselves
        ExecutorService executorService = Executors.newCachedThreadPool();
        int politenessDelayMilliseconds = ObjectUtils.firstNonNull(analyse.getPolitenessDelayMilliseconds(),
                DEFAULT_POLITENESS_DELAY_MILLISECONDS);
        int timeoutMilliseconds = ObjectUtils.firstNonNull(analyse.getTimeoutMilliseconds(), DEFAULT_TIMEOUT_MILLISECONDS);
        int pagesRequestedCounter = 0;
        StopWatch sw = new StopWatch("fetchResources");
        for (URI uri : uris) {
            if (!analyse.isCheckAvailability()) {
                results.put(uri, Optional.of("Validation is off"));
                continue;
            }
            if (analyse.getMaxVisitPages() <= pagesRequestedCounter) {
                results.put(uri, Optional.of("Max visit pages reached"));
                continue;
            }
            if (politenessDelayMilliseconds > 0) {
                sw.start("Politely waiting");
                try {
                    TimeUnit.MILLISECONDS.sleep(politenessDelayMilliseconds);
                } catch (InterruptedException e) {
                    break;
                }
                sw.stop();
            }
            executorService.execute(() -> results.put(uri, fetchResource(uri, timeoutMilliseconds)));
            pagesRequestedCounter++;
        }
        sw.start("Waiting for tasks to finish");
        try {
            executorService.shutdownNow();
            executorService.awaitTermination(ObjectUtils.firstNonNull(analyse.getGlobalTimeoutMilliseconds(),
                    DEFAULT_GLOBAL_TIMEOUT_MILLISECONDS), TimeUnit.MILLISECONDS);
        } catch (InterruptedException e) {
            for (URI uri : uris) {
                results.computeIfAbsent(uri, k -> Optional.of("Global timeout"));
            }
        } finally {
            sw.stop();
            executorService.shutdownNow();
        }
        LOGGER.info(sw.prettyPrint());
        return results;
    }

    /**
     * @param uri
     * @return errorMessage
     */
    Optional<String> fetchResource(URI uri, Integer timeout) {
        StopWatch sw = new StopWatch("Fetching single resources " + uri.toString());
        sw.start("Fetching resource");
        try {
            URLConnection urlConnection = uriUtils.openConnection(uri);

            HttpURLConnection connection = (HttpURLConnection) urlConnection;
            connection.setInstanceFollowRedirects(true);
            connection.setConnectTimeout(timeout);
            connection.setReadTimeout(timeout);
            connection.connect();

            if (connection.getResponseCode() != 200) {
                return Optional.of("Status " + connection.getResponseCode());
            }
            return Optional.empty();
        } catch (IOException e) {
            return Optional.of(e.getMessage());
        } finally {
            sw.stop();
            LOGGER.info(sw.prettyPrint());
        }
    }

    HtmlPage fetchHtmlPage(String url, Integer timeoutMilliseconds, WebClient webClient, boolean processJS) throws IOException {
        Page newPage = webClient.getPage(url);
        if (!(newPage instanceof HtmlPage)) {
            LOGGER.info("Some page that is not HTML:" + newPage.getClass().getName());
            return null;
        }
        HtmlPage page = (HtmlPage) newPage;
        waitForJavaScriptToLoad(page, timeoutMilliseconds, processJS);

        return page;
    }


    /**
     * JS could load for sometime.
     * Still we could not wait until it successfully finishes.
     * Many websites make calls to some statistic/analytics every second.
     * It would not stop ever.
     * <p>
     * That is why timeout is implemented based on timeout.
     *
     * @param page
     * @param processJS waits for js to finish if true
     * @return how many ms waited
     */
    long waitForJavaScriptToLoad(HtmlPage page, Integer timeoutMilliseconds, boolean processJS) {
        if (!processJS) {
            return 0;
        }

        timeoutMilliseconds = ObjectUtils.firstNonNull(timeoutMilliseconds, DEFAULT_TIMEOUT_MILLISECONDS);
        JavaScriptJobManager manager = page.getEnclosingWindow().getJobManager();
        long startedWaiting = System.currentTimeMillis();
        while (manager.getJobCount() > 0 && (startedWaiting + timeoutMilliseconds) >= System.currentTimeMillis()) {
            try {
                LOGGER.debug("JS still loading for page " + page.getUrl().toString());
                Thread.sleep(timeoutMilliseconds / 10);
            } catch (InterruptedException e) {
                LOGGER.error("Failed to wait finish of JS load for page " + page.getUrl().toString(), e);
            }
        }
        return System.currentTimeMillis() - startedWaiting;
    }

    private WebClient init( Integer timeoutMilliseconds, boolean processJS) {
        WebClient webClient = new WebClient();
        webClient.getOptions().setThrowExceptionOnScriptError(false);
        webClient.getOptions().setCssEnabled(false);
        webClient.getOptions().setTimeout(ObjectUtils.firstNonNull(timeoutMilliseconds, DEFAULT_TIMEOUT_MILLISECONDS));
        webClient.getOptions().setJavaScriptEnabled(processJS);
        webClient.getOptions().setRedirectEnabled(true);

        return webClient;
    }

}

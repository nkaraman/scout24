package com.immobilienscout24.technical.challenge.service;

import com.immobilienscout24.technical.challenge.dto.LinkedResourceResponse;
import org.apache.commons.lang3.tuple.Pair;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;


import java.net.URI;
import java.util.*;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;

/**
 * Created by astral on 23.07.17.
 */
@RunWith(MockitoJUnitRunner.class)
public class AnalysisServiceTest {
    private static final String HTTPS_URL = "https://google.com";
    public static final String NOT_HTTP_PROTOCOL = "ftp://random.de";

    @Mock
    private UrlFetcher urlFetcher;

    private AnalysisService analysisService;

    @Before
    public void setUp() throws Exception {
        analysisService = new AnalysisService(urlFetcher);
    }

    @Test
    public void checkLinks() throws Exception {
        Set<String> urls = new HashSet<>(Arrays.asList("https://mail.google.com",
                "https://google.com/some/service",
                "https://google.com?search=lifeIsGood",
                "https://not.a.g.com",
                "https://some.another.famous.website.com"));

        Map<URI, Optional<String>> urlToErrorMessageMap = new HashMap<>();
        for (String url : urls) {
            urlToErrorMessageMap.put(new URI(url), Optional.empty());
        }

        when(urlFetcher.fetchResources(any(), any())).thenReturn(urlToErrorMessageMap);

        Pair<List<LinkedResourceResponse>, List<LinkedResourceResponse>> result =
                analysisService.checkLinks(null, urls, "google.com");

        Assert.assertEquals(3, result.getLeft().size());
        Assert.assertEquals(2, result.getRight().size());
    }

    @Test
    public void buildHttpsUris() throws Exception {
        Pair<List<URI>, List<LinkedResourceResponse>> result
                = analysisService.buildHttpsUris(new HashSet<>(Arrays.asList(NOT_HTTP_PROTOCOL, HTTPS_URL)));
        Assert.assertEquals(1, result.getLeft().size());
        Assert.assertEquals(1, result.getRight().size());
    }

    @Test
    public void validateAndBuildUriNotSupportedProtocol() throws Exception {
        Assert.assertNull(analysisService.validateAndBuildUri(NOT_HTTP_PROTOCOL).getLeft());
        Assert.assertNotNull(analysisService.validateAndBuildUri(NOT_HTTP_PROTOCOL).getRight());
    }


    @Test
    public void validateAndBuildUriHTTP() throws Exception {
        Assert.assertNotNull(analysisService.validateAndBuildUri(HTTPS_URL).getLeft());
        Assert.assertNull(analysisService.validateAndBuildUri(HTTPS_URL).getRight());
    }

}
package com.immobilienscout24.technical.challenge.service;

import com.gargoylesoftware.htmlunit.Page;
import com.gargoylesoftware.htmlunit.WebClient;
import com.gargoylesoftware.htmlunit.WebWindow;
import com.gargoylesoftware.htmlunit.html.HtmlPage;
import com.gargoylesoftware.htmlunit.javascript.background.JavaScriptJobManager;
import com.immobilienscout24.technical.challenge.dto.AnalysisRequest;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import javax.net.ssl.HttpsURLConnection;
import java.net.URI;
import java.net.URL;
import java.net.URLConnection;
import java.util.Collections;
import java.util.Map;
import java.util.Optional;

import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class UrlFetcherTest {

    static final int TIMEOUT_MILLISECONDS = 1000;
    static final String URL = "http://not_a_random_website.com";
    @Mock
    private HtmlPage page;
    @Mock
    private WebWindow window;
    @Mock
    private JavaScriptJobManager javaScriptJobManager;
    @Mock
    private WebClient webClient;
    @Mock
    private Page notHtmlPage;
    @Mock
    private URLConnection urlConnection;
    @Mock
    private HttpsURLConnection httpsURLConnection;
    @Mock
    private URIUtils uriUtils;

    private URI uri;

    private UrlFetcher urlFetcher;

    @Before
    public void setUp() throws Exception {
        uri = new URI(URL);
        urlFetcher = new UrlFetcher(uriUtils);
    }

    @Test
    public void fetchResourcesWithAvailabilityOff() throws Exception {
        AnalysisRequest analysisRequest = new AnalysisRequest();
        Map<URI, Optional<String>> result = urlFetcher.fetchResources(Collections.singletonList(uri), analysisRequest);
        Assert.assertEquals("Validation is off", result.get(uri).get());
    }

    @Test
    public void fetchResourcesWithToManyReached() throws Exception {
        AnalysisRequest analysisRequest = new AnalysisRequest();
        analysisRequest.setCheckAvailability(true);
        analysisRequest.setMaxVisitPages(0);
        Map<URI, Optional<String>> result = urlFetcher.fetchResources(Collections.singletonList(uri), analysisRequest);
        Assert.assertEquals("Max visit pages reached", result.get(uri).get());
    }

    @Test
    public void fetchResource() throws Exception {
        when(uriUtils.openConnection(uri)).thenReturn(httpsURLConnection);
        when(httpsURLConnection.getResponseCode()).thenReturn(200);
        Optional<String> result = urlFetcher.fetchResource(uri, TIMEOUT_MILLISECONDS);
        Assert.assertFalse("Expected to have errorMessage when no https", result.isPresent());
    }

    @Test
    public void fetchResourceWithStatusCodeNull() throws Exception {
        when(uriUtils.openConnection(uri)).thenReturn(httpsURLConnection);
        Optional<String> result = urlFetcher.fetchResource(uri, TIMEOUT_MILLISECONDS);
        Assert.assertTrue("Expected to have errorMessage when no https", result.isPresent());
    }

    @Test
    public void fetchResourceNotHttps() throws Exception {
        when(uriUtils.openConnection(uri)).thenReturn(urlConnection);
        Optional<String> result = urlFetcher.fetchResource(uri, TIMEOUT_MILLISECONDS);
        Assert.assertTrue("Expected to have errorMessage when no https", result.isPresent());
    }

    @Test
    public void fetchHtmlPageNotHtmlPage() throws Exception {
        Mockito.when(webClient.getPage(Mockito.eq(URL))).thenReturn(notHtmlPage);
        Assert.assertEquals(null, urlFetcher.fetchHtmlPage(URL, 0, webClient, false));
    }

    @Test
    public void fetchHtmlPage() throws Exception {

        Mockito.when(webClient.getPage(Mockito.eq(URL))).thenReturn(page);
        Assert.assertEquals(page, urlFetcher.fetchHtmlPage(URL, 0, webClient, false));
    }

    @Test
    public void waitForJavaScriptToLoadWithNoDelay() throws Exception {
        Assert.assertEquals("Expected no delay with processJS = false", 0,
                urlFetcher.waitForJavaScriptToLoad(page, TIMEOUT_MILLISECONDS, false));
    }


    @Test
    public void waitForJavaScriptToLoadWithTimeout() throws Exception {
        when(page.getEnclosingWindow()).thenReturn(window);
        when(page.getUrl()).thenReturn(new URL("http://google.com"));

        when(window.getJobManager()).thenReturn(javaScriptJobManager);
        when(javaScriptJobManager.getJobCount()).thenReturn(10);

        long time = urlFetcher.waitForJavaScriptToLoad(page, TIMEOUT_MILLISECONDS, true);
        Assert.assertTrue("Difference between timeout and actual timeout is too high",
                Math.abs(time - TIMEOUT_MILLISECONDS) < 50);
    }

}
package com.immobilienscout24.technical.challenge.controller;

import com.immobilienscout24.technical.challenge.Application;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.context.WebApplicationContext;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;


@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
public class AnalysisControllerIT {

    private MockMvc mockMvc;

    @Autowired
    private WebApplicationContext webApplicationContext;

    @Before
    public void setUp() {
        this.mockMvc = webAppContextSetup(webApplicationContext).build();
    }

    @Test
    public void analyseTestLogin() throws Exception {
        mockMvc.perform(get("/").param("url", "https://www.spiegel.de/meinspiegel/login.html"))
                .andExpect(status().isOk())
                .andExpect(view().name("index"))
                .andExpect(model().attribute("isLoginForm", true))
                .andExpect(model().attribute("htmlVersion", "HTML -//W3C//DTD HTML 4.01 Transitional//EN"))
                .andExpect(model().attributeExists("url", "title", "headingCounter", "internalLinks",
                        "internalLinksNumber", "externalLinks", "externalLinksNumber"))
                .andExpect(model().attributeDoesNotExist("errorMessage"));

    }

    @Test
    public void analyseTestLogin2() throws Exception {
        mockMvc.perform(get("/").param("url", "https://sso.immobilienscout24.de/sso/login?appName=is24main&source=meinkontodropdown-login&sso_return=https://www.immobilienscout24.de/sso/login.go?source%3Dmeinkontodropdown-login%26returnUrl%3D/geschlossenerbereich/start.html?source%253Dmeinkontodropdown-login"))
                .andExpect(status().isOk())
                .andExpect(view().name("index"))
                .andExpect(model().attribute("isLoginForm", true))
                .andExpect(model().attribute("htmlVersion", "html"))
                .andExpect(model().attributeExists("url", "title", "headingCounter", "internalLinks",
                        "internalLinksNumber", "externalLinks", "externalLinksNumber"))
                .andExpect(model().attributeDoesNotExist("errorMessage"));

    }

    @Test
    public void analyseTestLogin3() throws Exception {
        mockMvc.perform(get("/").param("url", "http://github.com/login"))
                .andExpect(status().isOk())
                .andExpect(view().name("index"))
                .andExpect(model().attribute("isLoginForm", true))
                .andExpect(model().attribute("htmlVersion", "html"))
                .andExpect(model().attributeExists("url", "title", "headingCounter", "internalLinks",
                        "internalLinksNumber", "externalLinks", "externalLinksNumber"))
                .andExpect(model().attributeDoesNotExist("errorMessage"));
    }

    @Test
    public void analyseTestLogin4() throws Exception {
        mockMvc.perform(get("/").param("url", "http://www.axelspringer.de/en/index.html"))
                .andExpect(status().isOk())
                .andExpect(view().name("index"))
                .andExpect(model().attribute("isLoginForm", false))
                .andExpect(model().attribute("htmlVersion", "html -//W3C//DTD XHTML 1.0 Strict//EN"))
                .andExpect(model().attributeExists("url", "title", "headingCounter", "internalLinks",
                        "internalLinksNumber", "externalLinks", "externalLinksNumber"))
                .andExpect(model().attributeDoesNotExist("errorMessage"));
    }

    @Test
    public void analyseTestLogin5() throws Exception {
        mockMvc.perform(get("/").param("url", "https://www.facebook.com/"))
                .andExpect(status().isOk())
                .andExpect(view().name("index"))
                .andExpect(model().attribute("isLoginForm", true))
                .andExpect(model().attribute("htmlVersion", "html"))
                .andExpect(model().attributeExists("url", "title", "headingCounter", "internalLinks",
                        "internalLinksNumber", "externalLinks", "externalLinksNumber"))
                .andExpect(model().attributeDoesNotExist("errorMessage"));
    }

    @Configuration
    @Import(Application.class)
    public static class TestConfig {

    }

}
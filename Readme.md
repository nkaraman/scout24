Hello everyone,

----------------------------
How to build\start\test the App:
----------------------------
mvn clean package

// if you don't have maven
// ./mvnw clean package

java -jar target/challenge-0.0.1-SNAPSHOT.jar

// App would be available on 8080 port http://localhost:8080

-----------------------------
Project description
-----------------------------
The project is Spring Boot App with HtmlUnit as HTML\JS processor and Mustache as Template Engine.
JSoup is a nice library and html processor with a perfect API. 
However, it does not support JS processing. And it is very crucial nowadays. 
More and more websites require JS in order simply read the content.

-----------------------------
Assumptions
-----------------------------
I did not limit amount of threads whenever we query linked resources, because it is a test project
(politeness delay is implemented).

-----------------------------
Optional part: How to make better performance on linked resources checking
-----------------------------
Crucial part is that we could not query the same website too many times 
(and should follow robot.txt limitations).
HTML processing and analysis takes less than 5% of time. 

To make it reasonably faster and avoid blocks from websites. We could implement 
architecture with several small workers on separated IPs (better in separated networks).
And make some mixing/blending algorithm in order to give a worker urls from different
websites (to avoid robot.txt limits).

Some caching could be handy for linked resources coming from frequent resources such as
social networks, etc.


 
Thank you for reviewing.
Have a nice day :)


   